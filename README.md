# Scanslator

A tool for scanslating (translating scanned images, mostly manga)

This is a work in progress, but some features are already available. 
The tool is supposed to help with scanslating manga, by providing tools for editing images, as well as a neural network based speechbubble auto-colouring.

As of now the program is split into two parts: **detect.py** is setup to detect and empty the speechbubbles in the directory input.
**gui.py** is the work in progress implementation of the editing software itself, the features are currently limited to selecting the manga to edit and selecting the image

To run this on your machine, you will need python 3.7, as the Mask RCNN implementation i'm using (all rights reserved), only works on a tensorflow version supported by python 3.7. Next install the dependencies with `pip install -r requirements.txt` or equivelent. This should be enough to set everything up. To run the program(s): copy the folder from /examples into /inputs (if this folder doesn't exist create it), [NOTE: Copy the full folder and not the files inside of it, your input should look like input/folder/...], next run the programs `detect.py` and `gui.py`
