import os, cv2, numpy as np
from mrcnn.config import Config
from mrcnn import model as modellib, utils, visualize

class InferenceConfig(Config):
  NAME = "speechbubble"
  NUM_CLASSES = 1 + 1
  GPU_COUNT = 1
  IMAGES_PER_GPU = 1

if __name__ == '__main__':
  config = InferenceConfig()
  model = modellib.MaskRCNN(mode="inference", config=config, model_dir="output")
  model.load_weights("weights.h5", by_name=True)

  if not os.path.isdir('input'):
    os.mkdir('input')
  elif not os.path.isdir('output'):
    os.mkdir('output')

  os.chdir('input')
  data = []
  for root_dir, dirs, files in os.walk('.'):
    if len(files) > 0:
      path = os.path.join("../output/",root_dir)
      if not os.path.isdir(path):
        os.mkdir(path)
      data.append({'root':root_dir, 'images':[b for b in zip(files,[cv2.imread(os.path.join(root_dir, x),1) for x in files])]})
  os.chdir('../output')
   
  kernel = np.ones((8,8), np.uint8)
  for entry in data:
    os.chdir(entry['root'])
    for name, image in entry['images']:
      r = model.detect([image])[0]
      N = r['rois'].shape[0]
      mask = np.logical_or.reduce(r['masks'],axis=2).astype(np.uint8)
      white = np.zeros(image.shape, np.uint8)
      white.fill(255)
      mask = cv2.erode(mask,kernel).astype(np.uint8)
      for j in range(3):
        image[:,:,j] =  np.where(mask==0,image[:,:,j],white[:,:,j])
      cv2.imwrite(name, image)
    os.chdir('../')
      