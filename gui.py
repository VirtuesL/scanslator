from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import sys, os


class tile(QAbstractButton):
  def __init__(self, pixmap, location, callback):
    super(tile,self).__init__()
    self.pixmap = pixmap
    self.setToolTip(location)
    self.setFixedSize(pixmap.width(),pixmap.height())
    self.callback = callback

  def paintEvent(self, event):
    painter = QPainter(self)
    painter.setRenderHint(QPainter.SmoothPixmapTransform)
    painter.drawPixmap(event.rect(),self.pixmap)

  def sizeHint(self):
    return self.pixmap.size()

  def mousePressEvent(self, event):
    self.parent = self.parentWidget().parent()
    self.parent.replaceCentralWidget()
    self.callback(self.toolTip())

class imageEditor(QPainter):
  def __init__(self):
    pass


class mainWindow(QMainWindow):
  def __init__(self):
    super().__init__()
    self.setWindowTitle(u'Scanslator')
    self.resize(600,400)
    self.setCentralWidget(QWidget())
    self.grid = QGridLayout(self.centralWidget())
    self.grid.setDefaultPositioning(6,1)
    self.populateWindow('input')
    self.show()
  
  def replaceCentralWidget(self):
    self.centralWidget().destroy()
    self.setCentralWidget(QWidget())
    self.grid = QGridLayout(self.centralWidget())
    self.grid.setDefaultPositioning(6,1)

  def populateWindow(self, src):
    inp = os.walk(src)
    rd, dirs, fl = next(inp)
    
    if len(fl) == 0:
      for root_dir, _, files in inp:
        pixmap = QPixmap(os.path.join(root_dir,files[0])).scaledToHeight(384, Qt.SmoothTransformation)
        self.grid.addWidget(tile(pixmap,root_dir,self.populateWindow))
    else:
      inp = os.walk(src)
      for root_dir, _, files in inp:
        for image in sorted(files,key=lambda x: int(x.split('.')[0])):
          pixmap = QPixmap(os.path.join(root_dir,image)).scaledToHeight(384,Qt.SmoothTransformation)
          self.grid.addWidget(tile(pixmap, image, self.createDrawingBoard))
  
  def createDrawingBoard(self, image):
    print(image)


if __name__ == "__main__":
  app = QApplication(sys.argv)
  win = mainWindow()
  sys.exit(app.exec_())